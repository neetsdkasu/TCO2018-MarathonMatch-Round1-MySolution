Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim S As Integer = CInt(Console.ReadLine())
			Dim C As Integer = CInt(Console.ReadLine())
			Dim cities(C - 1) As Integer
			For i As Integer = 0 To UBound(cities)
				cities(i) = CInt(Console.ReadLine())
			Next i
			Dim junctionCost As Double = CDbl(Console.ReadLine())
			Dim failureProbability As Double = CDbl(Console.ReadLine())
            
			Dim raj As New RoadsAndJunctions()
			Dim junctions() As Integer = raj.buildJunctions( _
                        S, cities, junctionCost, failureProbability)

			Console.WriteLine(junctions.Length)
			For Each r As Integer In junctions
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
            
            Dim J As Integer = CInt(Console.ReadLine())
            Dim junctionStatus(J - 1) As Integer
            For i As Integer = 0 To UBound(junctionStatus)
                junctionStatus(i) = CInt(Console.ReadLine())
            Next i
            
            Dim roads() As Integer = raj.buildRoads(junctionStatus)
            
            Console.WriteLine(roads.Length)
            For Each r As Integer In roads
                Console.WriteLine(r)
            Next r
			Console.Out.Flush()
            
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module