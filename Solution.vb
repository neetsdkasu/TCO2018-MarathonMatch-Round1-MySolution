Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)
Imports TpD = System.Tuple(Of Double, Double)
Imports TpDI4D = System.Tuple(Of Double, Integer, Integer, Integer, Integer, Double)
Imports TpDI2 = System.Tuple(Of Double, Integer, Integer)
Imports TpDI = System.Tuple(Of Double, Integer)
Imports Pos = System.Int32

Public Class RoadsAndJunctions
    Private Const SHFT As Integer = 11
    Private Const MASK As Pos = (1 << SHFT) - 1
    Private Const HMASK As Pos = MASK << SHFT
    
    Private Function ToPos(x As Integer, y As Integer) As Pos
        ToPos = (y << SHFT) Or x
    End Function
    Private Function PX(p As Pos) As Integer
        PX = p And MASK
    End Function
    Private Function SX(p As Pos, x As Integer) As Integer
        SX = (p And HMASK) Or x
    End Function
    Private Function PY(p As Pos) As Integer
        PY = p >> SHFT
    End Function
    Private Function SY(p As Pos, y As Integer) As Integer
        SY = (y << SHFT) Or (p And MASK)
    End Function
    Private Function Dist(p1 As Pos, p2 As Pos) As Double
        Dim dx As Double = CDbl(PX(p1) - PX(p2))
        Dim dy As Double = CDbl(PY(p1) - PY(p2))
        Dist = Math.Sqrt(dx * dx + dy * dy)
    End Function
    Private Function Vec(f As Pos, t As Pos) As Tp
        Vec = New Tp(PX(t) - PX(f), PY(t) - PY(f))
    End Function
    Private Function DistD(p1 As Pos, p2 As TpD) As Double
        Dim dx As Double = CDbl(PX(p1)) - p2.Item1
        Dim dy As Double = CDbl(PY(p1)) - p2.Item2
        DistD = Math.Sqrt(dx * dx + dy * dy)
    End Function

    Dim rand As New Random(19831983)
    Dim S As Integer, NC As Integer, NJ As Integer
    Dim used As New HashSet(Of Pos)()
    Dim citiesPos() As Pos
    Dim citiesDist(,) As Double
    Dim Left As Integer, Right As Integer
    Dim Top As Integer, Bottom As Integer
    Dim time0 As Integer
    Dim timeLimit As Integer
    
    Dim juncPos As New List(Of Pos)()
    Dim juncDist As New List(Of Double())()
    
    Dim dxs() As Integer = {0, -1, 0, 1}
    Dim dys() As Integer = {1, 0, -1, 0}
    
    Dim dummy() As Integer
    
    Public Function buildJunctions( _
                S As Integer, cities() As Integer, _
                junctionCost As Double, failureProbability As Double) As Integer()
        time0 = Environment.TickCount
        timeLimit = time0 + 9600
        Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))
        
        
        Me.S = S
        NC = cities.Length \ 2
        Left = S
        Top = S
        ReDim citiesPos(NC - 1), citiesDist(NC - 1, NC - 1)
        For i As Integer = 0 To NC - 1
            Dim x As Integer = cities(i * 2)
            Dim y As Integer = cities(i * 2 + 1)
            citiesPos(i) = ToPos(x, y)
            Left   = Math.Min(Left,   x)
            Right  = Math.Max(Right,  x)
            Top    = Math.Min(Top,    y)
            Bottom = Math.Max(Bottom, y)
            used.Add(citiesPos(i))
        Next i
        For i As Integer = 0 To NC - 1
            For j As Integer = i + 1 To NC - 1
                Dim d As Double = Dist(citiesPos(i), Me.citiesPos(j))
                citiesDist(i, j) = d
                citiesDist(j, i) = d
            Next j
        Next i
        
        Console.Error.WriteLine("S: {0}, NC: {1}, cost: {2}, fail: {3}", _
            S, NC, junctionCost, failureProbability)
        Console.Error.WriteLine("Bound ({0}, {1}) - ({2}, {3})", _
            Left, Top, Right, Bottom)
            
        NJ = 0
        Dim roads As List(Of Tp) = MinimumSpanningTree(Nothing)
        Dim rmap(NC - 1, NC - 1) As Integer
        For Each r As Tp In roads
            rmap(r.Item1, r.Item2) = 1
            rmap(r.Item2, r.Item1) = 1
        Next r
        
        Dim needs As Integer = 1
        Dim cost As Double = CDbl(needs) * junctionCost
        
        Dim fp As List(Of TpDI4D) = FermatPoints(rmap)
        Dim rems As New List(Of TpDI)()
        fp.Sort()
        ' Dim total As Double = 0
        For Each p As TpDI4D In fp
            If used.Contains(p.Item2) Then Continue For
            Dim re As Integer = 0
            re += rmap(p.Item3, p.Item4)
            re += rmap(p.Item4, p.Item5)
            re += rmap(p.Item5, p.Item3)
            If re < 2 _
            OrElse p.Item1 > 0.0 _
            OrElse -p.Item1 * (1.0 - failureProbability) < cost Then
            ' OrElse total - p.Item1 * (1.0 - failureProbability) < cost Then
                rems.Add(New TpDI(p.Item6, p.Item2))
                Continue For
            End If
            rmap(p.Item3, p.Item4) = 0
            rmap(p.Item4, p.Item3) = 0
            rmap(p.Item4, p.Item5) = 0
            rmap(p.Item5, p.Item4) = 0
            rmap(p.Item5, p.Item3) = 0
            rmap(p.Item3, p.Item5) = 0
            used.Add(p.Item2)
            Dim ds(NC + juncDist.Count - 1) As Double
            For j As Integer = 0 To NC - 1
                ds(j) = Dist(citiesPos(j), p.Item2)
            Next j
            For j As Integer = 0 To juncPos.Count - 1
                ds(NC + j) = Dist(juncPos(j), p.Item2)
            Next j
            juncPos.Add(p.Item2)
            juncDist.Add(ds)
            ' total += -p.Item1 * (1.0 - failureProbability) - cost
            If juncPos.Count = NC * 2 Then Exit For
        Next p
        
        NJ = juncPos.Count
        
        ReDim dummy(NC * 2 - 1)
        For i As Integer = 0 To UBound(dummy)
            dummy(i) = 1
        Next i

        Dim sc As Double = CalcScore()
        Console.Error.WriteLine("sc? {0}", sc)
        
        ' For i As Integer = 0 To rems.Count - 1
            ' Dim j As Integer = rand.Next(i, rems.Count)
            ' Dim t As TpDI4 = rems(i)
            ' rems(i) = rems(j)
            ' rems(j) = t
        ' Next i
        rems.Sort()
            
        Dim flag(rems.Count - 1) As Boolean
        Dim find As Integer = 0
        Dim cycle As Integer = 0
        NJ += 1
        juncPos.Add(ToPos(S + 1, S + 1))
        juncDist.Add(New Double(){})
        Do While NJ < NC * 2
            Dim bf As Integer = find
            cycle += 1
            For i As Integer = 0 To rems.Count - 1
                If flag(i) Then Continue For
                Dim time1 As Integer = Environment.TickCount
                If time1 > timeLimit Then
                    Console.Error.WriteLine("cycle: {0}", cycle)
                    Console.Error.WriteLine("lp: {0} / {1}", i, rems.Count)
                    Console.Error.WriteLine("find: {0}", find)
                    Exit Do
                End If
                Dim p As TpDI = rems(i)
                If used.Contains(p.Item2) Then
                    flag(i) = True
                    Continue For
                End If
                Dim ds(NC + NJ - 2) As Double
                For j As Integer = 0 To NC - 1
                    ds(j) = Dist(citiesPos(j), p.Item2)
                Next j
                For j As Integer = 0 To NJ - 2
                    ds(NC + j) = Dist(juncPos(j), p.Item2)
                Next j
                juncPos(NJ - 1) = p.Item2
                juncDist(NJ - 1) = ds
                Dim tsc As Double = CalcScore()
                If tsc >= sc Then Continue For
                ' If total + (sc - tsc) * (1.0 - failureProbability) < cost Then
                If (sc - tsc) * (1.0 - failureProbability) < cost Then
                    Continue For
                End If
                flag(i) = True
                used.Add(p.Item2)
                NJ += 1
                juncPos.Add(ToPos(S + 1, S + 1))
                juncDist.Add(New Double(){})
                ' total += (sc - tsc) * (1.0 - failureProbability) - cost
                sc = tsc
                find += 1
                Exit For
            Next i
            If bf = find Then
                Console.Error.WriteLine("cycle: {0}", cycle)
                Console.Error.WriteLine("find: {0}", find)
                Exit Do
            End If
        Loop
        
        'random point
        find = 0
        cycle = 0
        Dim hits As Integer = 0
        Dim time2 As Integer = Environment.TickCount
        Dim time3 As Integer = time2
        Dim timeDiff As Integer = 0
        Do While NJ < NC * 2
            time3 = time2
            time2 = Environment.TickCount
            timeDiff = Math.Max(timeDiff, time2 - time3)
            If time2 + timeDiff > timeLimit Then
                Console.Error.WriteLine("cycle2: {0} / {1}", hits, cycle)
                Console.Error.WriteLine("find: {0}", find)
                Exit Do
            End If
            cycle += 1
            Dim x As Integer = rand.Next(Left, Right + 1)
            Dim y As Integer = rand.Next(Top, Bottom + 1)
            Dim p As Pos = ToPos(x, y)
            If used.Contains(p) Then Continue Do
            hits += 1
            Dim ds(NC + NJ - 2) As Double
            For j As Integer = 0 To NC - 1
                ds(j) = Dist(citiesPos(j), p)
            Next j
            For j As Integer = 0 To NJ - 2
                ds(NC + j) = Dist(juncPos(j), p)
            Next j
            juncPos(NJ - 1) = p
            juncDist(NJ - 1) = ds
            Dim tsc As Double = CalcScore()
            If tsc >= sc Then Continue Do
            ' If total + (sc - tsc) * (1.0 - failureProbability) < cost Then
            If (sc - tsc) * (1.0 - failureProbability) < cost Then
                Continue Do
            End If
            used.Add(p)
            NJ += 1
            juncPos.Add(ToPos(S + 1, S + 1))
            juncDist.Add(New Double(){})
            ' total += (sc - tsc) * (1.0 - failureProbability) - cost
            sc = tsc
            find += 1
        Loop        
        
        NJ -= 1
        
        Dim ret(NJ * 2 - 1) As Integer
        For i As Integer = 0 To NJ - 1
            Dim f As Pos = juncPos(i)
            ret(i * 2)     = PX(f)
            ret(i * 2 + 1) = PY(f)
        Next i
        
        buildJunctions = ret
        
    End Function
    
    Public Function buildRoads(junctionStatus() As Integer) As Integer()
        
        Dim roads As List(Of Tp) = MinimumSpanningTree(junctionStatus)
        
        Dim ret(roads.Count * 2 - 1) As Integer
        For i As Integer = 0 To roads.Count - 1
            ret(i * 2)     = roads(i).Item1
            ret(i * 2 + 1) = roads(i).Item2
            ' Console.Error.WriteLine("road: {0}", roads(i))
        Next i
        
        buildRoads = ret
        
        Dim endtime As Integer = Environment.TickCount
        Console.Error.WriteLine("endtime: {0} ms", endtime - time0)
        
    End Function
    
    Private Function FermatPoints(rmap(,) As Integer) As List(Of TpDI4D)
        Dim ret As New List(Of TpDI4D)()
        Dim cnt As Integer = 0
        Dim vs(NC - 1, NC - 1) As Tp
        For i As Integer = 0 To NC - 2
            For j As Integer = i + 1 TO NC - 1
                vs(i, j) = Vec(citiesPos(i), citiesPos(j))
                vs(j, i) = Vec(citiesPos(j), citiesPos(i))
            Next j
        Next i
        For i As Integer = 0 To NC - 3
            For j As Integer = i + 1 To NC - 2
                Dim da As Double = citiesDist(i, j)
                For k As Integer = j + 1 To NC - 1
                    Dim db As Double = citiesDist(j, k)
                    Dim dc As Double = citiesDist(i, k)
                    Dim ta As Double = CosineA(da, db, dc)
                    If ta <= -0.5 Then Continue For
                    Dim tb As Double = CosineA(db, da, dc)
                    If tb <= -0.5 Then Continue For
                    Dim tc As Double = CosineA(dc, db, da)
                    If tc <= -0.5 Then Continue For
                    Dim ok As Boolean = True
                    For u As Integer = 0 To NC - 1
                        If u = i OrElse u = j OrElse u = k Then Continue For
                        Dim vij As Tp = vs(i, j)
                        Dim vju As Tp = vs(j, u)
                        Dim cp1 As Integer = CrossProductZ(vij, vju)
                        Dim vjk As Tp = vs(j, k)
                        Dim vku As Tp = vs(k, u)
                        Dim cp2 As Integer = CrossProductZ(vjk, vku)
                        If cp1 <> cp2 Then
                            Continue For
                        End If
                        Dim vki As Tp = vs(k, i)
                        Dim viu As Tp = vs(i, u)
                        Dim cp3 As Integer = CrossProductZ(vki, viu)
                        If cp1 <> cp3 Then
                            Continue For
                        End If
                        ok = False
                        Exit For
                    Next u
                    If Not ok Then Continue For
                    Dim v1 As TpD = Rotate(vs(i, j), citiesPos(i))
                    Dim v2 As TpD = Rotate(vs(j, i), citiesPos(j))
                    Dim d1 As Double = DistD(citiesPos(k), v1)
                    Dim d2 As Double = DistD(citiesPos(k), v2)
                    Dim v3 As TpD = Rotate(vs(j, k), citiesPos(j))
                    Dim v4 As TpD = Rotate(vs(k, j), citiesPos(k))
                    Dim d3 As Double = DistD(citiesPos(i), v3)
                    Dim d4 As Double = DistD(citiesPos(i), v4)
                    If d2 > d1 Then v1 = v2
                    If d4 > d3 Then v3 = v4
                    Dim ip As Pos = Intersection(v1, citiesPos(k), v3, citiesPos(i))
                    If used.Contains(ip) Then Continue For
                    Dim c1 As Double = 0.0
                    c1 += CDbl(rmap(i, j)) * citiesDist(i, j)
                    c1 += CDbl(rmap(j, k)) * citiesDist(j, k)
                    c1 += CDbl(rmap(k, i)) * citiesDist(k, i)
                    Dim c2 As Double = 0.0
                    c2 += Dist(ip, citiesPos(i))
                    c2 += Dist(ip, citiesPos(j))
                    c2 += Dist(ip, citiesPos(k))
                    ret.Add(New TpDI4D(c2 - c1, ip, i, j, k, c2))
                    cnt += 1
                Next k
            Next j
        Next i
        Console.Error.WriteLine("fermat points: {0} / {1}", _
            cnt, NC * (NC - 1) * (NC - 2) \ 6)
        FermatPoints = ret
    End Function
    
    Dim sin60 As Double = Math.Sqrt(2.0) * 0.5
    Const cos60 As Double = 0.5
    Private Function Rotate(p As Tp, m As Pos) As TpD
        Dim x As Double = CDbl(p.Item1)
        Dim y As Double = CDbl(p.Item2)
        Rotate = New TpD( _
            x * cos60 - y * sin60 + CDbl(PX(m)), _
            x * sin60 + y * cos60 + CDbl(PY(m)))
    End Function
    
    Private Function CosineA(da As Double, db As Double, dc As Double) As Double
        CosineA = (db * db + dc * dc - da * da) / (2.0 * db * dc)
    End Function
    
    Private Function CrossProductZ(va As Tp, vb As Tp) As Integer
        CrossProductZ = Math.Sign(va.Item1 * vb.Item2 - va.Item2 * vb.Item1)
    End Function
    
    Private Function Intersection(pA As TpD, pB As Pos, pC As TpD, pD As Pos) As Pos
        Dim xAB As Double = CDbl(PX(pB)) - pA.Item1
        Dim yAB As Double = CDbl(PY(pB)) - pA.Item2
        Dim xCD As Double = CDbl(PX(pD)) - pC.Item1
        Dim yCD As Double = CDbl(PY(pD)) - pC.Item2 
        Dim xCA As Double = pA.Item1 - pC.Item1
        Dim yCA As Double = pA.Item2 - pC.Item2
        Dim k As Double = (yCA * xCD - xCA * yCD) / (xAB * yCD - yAB * xCD)
        Dim x As Integer = CInt(Math.Round(k * xAB + pA.Item1))
        Dim y As Integer = CInt(Math.Round(k * yAB + pA.Item2))
        Intersection = ToPos(x, y)
    End Function
    
    Private Function FindMinPair(junctionStatus() As Integer) As Tuple(Of Tp, Double)
        Dim s1 As Integer = -1
        Dim s2 As Integer = -1
        Dim md As Double = Double.MaxValue
        For i As Integer = 0 To NC - 1
            For j As Integer = i + 1 To NC - 1
                If citiesDist(i, j) < md Then
                    s1 = i
                    s2 = j
                    md = citiesDist(i, j)
                End If
            Next j
        Next i
        For i As Integer = 0 To NJ - 1
            If junctionStatus(i) = 0 Then Continue For
            Dim ds() As Double = juncDist(i)
            For j As Integer = 0 To NC - 1
                If ds(j) < md Then
                    s1 = j
                    s2 = NC + i
                    md = ds(j)
                End If
            Next j
            For j As Integer = NC To UBound(ds)
                If junctionStatus(j - NC) = 0 Then Continue For
                If ds(j) < md Then
                    s1 = j
                    s2 = NC + i
                    md = ds(j)
                End If
            Next j
        Next i
        If s1 < 0 Then Throw New Exception("hoge")
        FindMinPair = New Tuple(Of Tp, Double)(New Tp(s1, s2), md)
    End Function
    
    Private Function MinimumSpanningTree(junctionStatus() As Integer) As List(Of Tp)
        Dim ret As New List(Of Tp)()
        Dim flag(NC + NJ - 1) As Boolean
        Dim minPair As Tp = FindMinPair(junctionStatus).Item1
        flag(minPair.Item1) = True
        flag(minPair.Item2) = True
        ret.Add(minPair)
        Do
            Dim s1 As Integer = -1
            Dim s2 As Integer = -1
            Dim md As Double = Double.MaxValue
            For i As Integer = 0 To NC - 1
                If Not flag(i) Then Continue For
                For j As Integer = 0 To NC - 1
                    If flag(j) OrElse i = j Then Continue For
                    If citiesDist(i, j) < md Then
                        s1 = i
                        s2 = j
                        md = citiesDist(i, j)
                    End If
                Next j
            Next i
            For i As Integer = 0 To NJ - 1
                If junctionStatus(i) = 0 Then Continue For
                Dim fi As Boolean = flag(NC + i)
                Dim ds() As Double = juncDist(i)
                For j As Integer = 0 To NC - 1
                    If flag(j) = fi Then Continue For
                    If ds(j) < md Then
                        s1 = j
                        s2 = NC + i
                        md = ds(j)
                    End If
                Next j
                For j As Integer = NC To UBound(ds)
                    If junctionStatus(j - NC) = 0 Then Continue For
                    If flag(j) = fi Then Continue For
                    If ds(j) < md Then
                        s1 = j
                        s2 = NC + i
                        md = ds(j)
                    End If
                Next j
            Next i
            If s1 < 0 Then Exit Do
            flag(s1) = True
            flag(s2) = True
            ret.Add(New Tp(s1, s2))
        Loop
        MinimumSpanningTree = ret
    End Function

    Private Function CalcScore() As Double
        Dim flag(NC + NJ - 1) As Boolean
        Dim minPairT As Tuple(Of Tp, Double) = FindMinPair(dummy)
        Dim minPair As Tp = minPairT.Item1
        Dim score As Double = minPairT.Item2
        flag(minPair.Item1) = True
        flag(minPair.Item2) = True
        Do
            Dim s1 As Integer = -1
            Dim s2 As Integer = -1
            Dim md As Double = Double.MaxValue
            For i As Integer = 0 To NC - 1
                If Not flag(i) Then Continue For
                For j As Integer = 0 To NC - 1
                    If flag(j) OrElse i = j Then Continue For
                    If citiesDist(i, j) < md Then
                        s1 = i
                        s2 = j
                        md = citiesDist(i, j)
                    End If
                Next j
            Next i
            For i As Integer = 0 To NJ - 1
                ' If junctionStatus(i) = 0 Then Continue For
                Dim fi As Boolean = flag(NC + i)
                Dim ds() As Double = juncDist(i)
                For j As Integer = 0 To NC - 1
                    If flag(j) = fi Then Continue For
                    If ds(j) < md Then
                        s1 = j
                        s2 = NC + i
                        md = ds(j)
                    End If
                Next j
                For j As Integer = NC To UBound(ds)
                    ' If junctionStatus(j - NC) = 0 Then Continue For
                    If flag(j) = fi Then Continue For
                    If ds(j) < md Then
                        s1 = j
                        s2 = NC + i
                        md = ds(j)
                    End If
                Next j
            Next i
            If s1 < 0 Then Exit Do
            flag(s1) = True
            flag(s2) = True
            score += md
        Loop
        CalcScore = score
    End Function
        
End Class