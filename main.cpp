#include "solution.cpp"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
    try {
        int S, C;
        double junctionCost;
        double failureProbability;
        cin >> S >> C;
        vector<int> cities(C);
        for (int i = 0; i < C; i++) {
            cin >> cities[i];
        }
        cin >> junctionCost >> failureProbability;
        RoadsAndJunctions raj;
        vector<int> junctions = raj.buildJunctions(S, cities, junctionCost, failureProbability);
        cout << junctions.size() << endl;
        for (auto it = junctions.begin(); it != junctions.end(); it++) {
            cout << *it << endl;
        }
        cout.flush();
        int J;
        cin >> J;
        vector<int> junctionStatus(J);
        for (int i = 0; i < J; i++) {
            cin >> junctionStatus[i];
        }
        vector<int> roads = raj.buildRoads(junctionStatus);
        cout << roads.size() << endl;
        for (auto it = roads.begin(); it != roads.end(); it++) {
            cout << *it << endl;
        }
        cout.flush();
    } catch (...) {
        cerr << "error!?" << endl;
    }
}