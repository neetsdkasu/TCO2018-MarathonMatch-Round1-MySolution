#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>
#include <cmath>
#include <random>

#ifdef _MSC_VER
#include <chrono>
inline auto get_time() {
    return std::chrono::system_clock::now();
}
inline auto to_msec(int t) {
    return std::chrono::milliseconds(t);
}
using Time = std::chrono::system_clock::time_point;
#else
const double ticks_per_sec = 2800000000;
inline double get_time() {
    uint32_t lo, hi;
    asm volatile ("rdtsc" : "=a" (lo), "=d" (hi));
    return (((uint64_t)hi << 32) | lo) / ticks_per_sec;
}
inline double to_msec(int t) {
    return double(t) / 1000.0;
}
using Time = double;
#endif

using namespace std;

using Tp     = pair<int, int>;
using TpD    = pair<double, double>;
using TpDI   = pair<double, int>;
using Table  = vector<vector<double> >;
using ITable = vector<vector<int> >;
using Pos    = int;

using TpDI4D = pair<double, 
    pair<pair<Tp, Tp>, double> >;

const int SHFT = 11;
const int MASK = (1 << SHFT) - 1;
const int HMASK = MASK << SHFT;

inline int ToPos(int x, int y) {
    return (y << SHFT) | x;
}
inline int PX(Pos p) {
    return p & MASK;
}
inline int SX(Pos p, int x) {
    return (p & HMASK) | x;
}
inline int PY(Pos p) {
    return p >> SHFT;
}
inline int SY(Pos p, int y) {
    return (y < SHFT) | (p & MASK);
}
inline double Dist(Pos p1, Pos p2) {
    double dx = (double)(PX(p1) - PX(p2));
    double dy = (double)(PY(p1) - PY(p2));
    return sqrt(dx * dx + dy * dy);
}
inline Tp Vec(Pos f, Pos t) {
    return Tp(PX(t) - PX(f), PY(t) - PY(f));
}
inline double DistD(Pos p1, TpD p2) {
    double dx = (double)PX(p1) - p2.first;
    double dy = (double)PY(p1) - p2.second;
    return sqrt(dx * dx + dy * dy);
}

inline double CosineA(double da, double db, double dc) {
    return (db * db + dc * dc - da * da) / (2.0 * db * dc);
}

inline int iSign(int v) {
    return v != 0 ? (v < 0 ? -1 : 1) : 0;
}

inline int CrossProductZ(Tp &va, Tp &vb) {
    return iSign(va.first * vb.second - va.second * vb.first);
}

const double sin60 = sqrt(2.0) * 0.5;
const double cos60 = 0.5;
inline TpD Rotate(Tp &p, Pos m) {
    auto x = double(p.first);
    auto y = double(p.second);
    return TpD(
        x * cos60 - y * sin60 + double(PX(m)),
        x * sin60 + y * cos60 + double(PY(m)));
}

inline Pos Intersection(TpD &pA, Pos pB, TpD &pC, Pos pD) {
    auto xAB = double(PX(pB)) - pA.first;
    auto yAB = double(PY(pB)) - pA.second;
    auto xCD = double (PX(pD)) - pC.first;
    auto yCD = double(PY(pD)) - pC.second;
    auto xCA = pA.first - pC.first;
    auto yCA = pA.second - pC.second;
    auto k = (yCA * xCD - xCA * yCD) / (xAB * yCD - yAB * xCD);
    int x = int(round(k * xAB + pA.first));
    int y = int(round(k * yAB + pA.second));
    return ToPos(x, y);
}

class RoadsAndJunctions {
    int NC, S, NJ;
    int Left, Top, Right, Bottom;
    vector<Pos> citiesPos, juncPos;
    Table citiesDist, juncDist;
    set<Pos> used;
    
    vector<int> dummy;
    vector<Tp> MinimumSpanningTree(vector<int> &junctionStatus);
    pair<Tp, double> FindMinPair(vector<int> &junctionStatus);
    vector<TpDI4D> FermatPoints(ITable rmap);
    double CalcScore();
    
    mt19937 gen;
    int randNext(int x) { return gen() % x; }
public:
    RoadsAndJunctions() : gen(19831983) {}
    vector<int> buildJunctions(int S, vector<int> cities, double junctionCost, double failureProbability);
    vector<int> buildRoads(vector<int> junctionStatus);
};


vector<int> RoadsAndJunctions::buildJunctions(int S, vector<int> cities, double junctionCost, double failureProbability) {
    auto timeLimit = get_time() + to_msec(9600);
    
    this->S = S;
    NC = cities.size() / 2;
    Right = Bottom = 0;
    Left = Top = S;
    citiesPos.resize(NC);
    citiesDist.resize(NC);
    for (int i = 0; i < NC; i++) {
        citiesDist[i].resize(NC);
        int x = cities[i * 2];
        int y = cities[i * 2 + 1];
        citiesPos[i] = ToPos(x, y);
        Left   = min(Left,   x);
        Right  = max(Right,  x);
        Top    = min(Top,    y);
        Bottom = max(Bottom, y);
        used.insert(citiesPos[i]);
    }
    for (int i = 0; i < NC; i++) {
        for (int j = i + 1; j < NC; j++) {
            auto d = Dist(citiesPos[i], citiesPos[j]);
            citiesDist[i][j] = d;
            citiesDist[j][i] = d;
        }
    }
    
    cerr << "S: "    << S                  << ", "
         << "NC: "   << NC                 << ", "
         << "cost: " << junctionCost       << ", "
         << "fail: " << failureProbability
         << endl;
    cerr << "Bound: "
         << "(" << Left  << ", " << Top    << ") - "
         << "(" << Right << ", " << Bottom << ")"
         << endl;
    
    dummy.resize(NC * 2);
    fill(dummy.begin(), dummy.end(), 1);
    
    NJ = 0;
    auto roads = MinimumSpanningTree(dummy);
    ITable rmap(NC);
    for (int i = 0; i < NC; i++) { rmap[i].resize(NC); }
    for (auto r = roads.begin(); r != roads.end(); r++) {
        rmap[r->first][r->second] = 1;
        rmap[r->second][r->first] = 1;
        // cerr << "path: "
             // << r->first << " - " << r->second
             // << endl;
    }
    
    int  needs = 1;
    auto cost  = double(needs) * junctionCost;
    auto fp    = FermatPoints(rmap);
    vector<TpDI> rems;
    sort(fp.begin(), fp.end());
    
    for (auto it = fp.begin(); it != fp.end(); it++) {
        auto &e2 = it->second;
        auto &e3 = e2.first;
        Pos p = Pos(e3.first.first);
        if (used.find(p) != used.end()) { continue; }
        int t1 = e3.first.second;
        int t2 = e3.second.first;
        int t3 = e3.second.second;
        int re = 0;
        re += rmap[t1][t2];
        re += rmap[t2][t3];
        re += rmap[t3][t1];
        if (re < 2
            || it->first > 0.0
            || -it->first * (1.0 - failureProbability) < cost) {
            rems.push_back({e2.second, p});
            continue;
        }
        rmap[t1][t2] = 0;
        rmap[t2][t1] = 0;
        rmap[t2][t3] = 0;
        rmap[t3][t2] = 0;
        rmap[t3][t1] = 0;
        rmap[t1][t3] = 0;
        used.insert(p);
        vector<double> ds(NC + juncDist.size());
        for (int j = 0; j < NC; j++) {
            ds[j] = Dist(citiesPos[j], p);
        }
        for (int j = 0; j < juncPos.size(); j++) {
            ds[NC + j] = Dist(juncPos[j], p);
        }
        juncPos.push_back(p);
        juncDist.push_back(ds);
        if (juncPos.size() == NC * 2) { break; }
    }
    
    NJ = juncPos.size();
    
    auto sc = CalcScore();
    cerr << "sc? " << sc << endl;
    
    sort(rems.begin(), rems.end());
    
    auto flag = new bool[rems.size()];
    fill(flag, flag + rems.size(), false);
    int find = 0;
    int cycle = 0;
    NJ++;
    juncPos.push_back(ToPos(S + 1, S + 1));
    juncDist.push_back(vector<double>());
    
    while (NJ < NC * 2) {
        int bf = find;
        cycle++;
        for (int i = 0; i < rems.size(); i++) {
            if (flag[i]) { continue; }
            auto time1 = get_time();
            if (time1 > timeLimit) {
                cerr << "cycle: " << cycle << endl;
                cerr << "lp: " << i << " / " << rems.size() << endl;
                cerr << "find: " << find << endl;
                goto rems_cycle_end;
            }
            Pos p = rems[i].second;
            if (used.find(p) != used.end()) {
                flag[i] = true;
                continue;
            }
            vector<double> ds(NC + NJ - 1);
            for (int j = 0; j < NC; j++) {
                ds[j] = Dist(citiesPos[j], p);
            }
            for (int j = 0; j < NJ - 1; j++) {
                ds[NC + j] = Dist(juncPos[j], p);
            }
            juncPos[NJ - 1] = p;
            swap(juncDist[NJ - 1], ds);
            auto tsc = CalcScore();
            if (tsc >= sc) { continue; }
            if ((sc - tsc) * (1.0 - failureProbability) < cost) {
                continue;
            }
            flag[i] = true;
            used.insert(p);
            NJ++;
            juncPos.push_back(ToPos(S + 1, S + 1));
            juncDist.push_back(vector<double>());
            sc = tsc;
            find++;
            break;
        }
        if (bf == find) {
            cerr << "cycle: " << cycle << endl;
            cerr << "find: " << find << endl;
            break;
        }
    }
rems_cycle_end:
    
    // random point
    find = 0;
    cycle = 0;
    int hits = 0;
    auto time2 = get_time();
    auto time3 = time2;
    auto timeDiff = time2 - time3;
    
    while (NJ < NC * 2) {
        time3 = time2;
        time2 = get_time();
        auto tmpDiff = time2 - time3;
        if (tmpDiff > timeDiff) {
            timeDiff = tmpDiff;
        }
        if (time2 + timeDiff > timeLimit) {
            cerr << "cycle2: " << hits << " / " << cycle << endl;
            cerr << "find: " << find << endl;
            break;
        }
        cycle++;
        int x = randNext(Right - Left + 1) + Left;
        int y = randNext(Bottom - Top + 1) + Top;
        Pos p = ToPos(x, y);
        if (used.find(p) != used.end()) { continue; }
        hits++;
        vector<double> ds(NC + NJ - 1);
        for (int j = 0; j < NC; j++) {
            ds[j] = Dist(citiesPos[j], p);
        }
        for (int j = 0; j < NJ - 1; j++) {
            ds[NC + j] = Dist(juncPos[j], p);
        }
        juncPos[NJ - 1] = p;
        swap(juncDist[NJ - 1], ds);
        auto tsc = CalcScore();
        if (tsc >= sc) { continue; }
        if ((sc - tsc) * (1.0 - failureProbability) < cost) {
            continue;
        }
        used.insert(p);
        NJ++;
        juncPos.push_back(ToPos(S + 1, S + 1));
        juncDist.push_back(vector<double>());
        sc = tsc;
        find++;
    }
    
    NJ--;
    
    vector<int> ret(NJ * 2);
    for (int i = 0; i < NJ; i++) {
        Pos f = juncPos[i];
        ret[i * 2]     = PX(f);
        ret[i * 2 + 1] = PY(f);
    }
    
    delete [] flag;
    return ret;
}

vector<int> RoadsAndJunctions::buildRoads(vector<int> junctionStatus) {
    
    auto roads = MinimumSpanningTree(junctionStatus);
    
    vector<int> ret(roads.size() * 2);
    for (int i = 0; i <roads.size(); i++) {
        ret[i * 2]     = roads[i].first;
        ret[i * 2 + 1] = roads[i].second;
    }

    return ret;
}

pair<Tp, double> RoadsAndJunctions::FindMinPair(vector<int> &junctionStatus) {
    int s1 = -1, s2 = -1;
    double md = 1e10;
    for (int i = 0; i < NC; i++) {
        for (int j = i + 1; j < NC; j++) {
            if (citiesDist[i][j] < md) {
                s1 = i;
                s2 = j;
                md = citiesDist[i][j];
            }
        }
    }
    for (int i = 0; i < NJ; i++) {
        if (junctionStatus[i] == 0) { continue; }
        auto &ds = juncDist[i];
        for (int j = 0; j < NC; j++) {
            if (ds[j] < md) {
                s1 = j;
                s2 = NC + i;
                md = ds[j];
            }
        }
        for (int j = NC; j < ds.size(); j++) {
            if (junctionStatus[j - NC] == 0) { continue; }
            if (ds[j] < md) {
                s1 = j;
                s2 = NC + i;
                md = ds[j];
            }
        }
    }
    if (s1 < 0) { throw "hoge"; }
    return pair<Tp, double>(Tp(s1, s2), md);
}

vector<Tp> RoadsAndJunctions::MinimumSpanningTree(vector<int> &junctionStatus) {
    vector<Tp> ret;
    auto flag = new bool[NC + NJ];
    fill(flag, flag + (NC + NJ), false);
    Tp minPair = FindMinPair(junctionStatus).first;
    flag[minPair.first] = true;
    flag[minPair.second] = true;
    ret.push_back(minPair);
    for (;;) {
        int s1 = -1, s2 = -1;
        double md = 1e10;
        for (int i = 0; i < NC; i++) {
            if (!flag[i]) { continue; }
            for (int j = 0; j < NC; j++) {
                if (flag[j] || i == j) { continue; }
                if (citiesDist[i][j] < md) {
                    s1 = i;
                    s2 = j;
                    md = citiesDist[i][j];
                }
            }
        }
        for (int i = 0; i < NJ; i++) {
            if (junctionStatus[i] == 0) { continue; }
            bool fi = flag[NC + i];
            auto &ds = juncDist[i];
            for (int j = 0; j < NC; j++) {
                if (flag[j] == fi) { continue; }
                if (ds[j] < md) {
                    s1 = j;
                    s2 = NC + i;
                    md = ds[j];
                }
            }
            for (int j = NC; j < ds.size(); j++) {
                if (junctionStatus[j - NC] == 0) { continue; }
                if (flag[j] == fi) { continue; }
                if (ds[j] < md) {
                    s1 = j;
                    s2 = NC + i;
                    md = ds[j];
                }
            }
        }
        if (s1 < 0) { break; }
        flag[s1] = true;
        flag[s2] = true;
        ret.push_back({s1, s2});
    }
    delete [] flag;
    return ret;
}

vector<TpDI4D> RoadsAndJunctions::FermatPoints(ITable rmap) {
    vector<TpDI4D> ret;
    int cnt = 0;
    vector<vector<Tp> > vs(NC);
    for (int i = 0; i < NC; i++) { vs[i].resize(NC); }
    for (int i = 0; i < NC; i++) {
        for (int j = i + 1; j < NC; j++) {
            vs[i][j] = Vec(citiesPos[i], citiesPos[j]);
            vs[j][i] = Vec(citiesPos[j], citiesPos[i]);
        }
    }
    for (int i = 0; i < NC; i++) {
        for (int j = i + 1; j < NC; j++) {
            auto da = citiesDist[i][j];
            for (int k = j + 1; k < NC; k++) {
                auto db = citiesDist[i][k];
                auto dc = citiesDist[j][k];
                auto ta = CosineA(da, db, dc);
                if (ta <= -0.5) { continue; }
                auto tb = CosineA(db, da, dc);
                if (tb <= -0.5) { continue; }
                auto tc = CosineA(dc, db, da);
                if (tc <= -0.5) { continue; }
                bool ok = true;
                Tp &vij = vs[i][j];
                Tp &vjk = vs[j][k];
                Tp &vki = vs[k][i];
                for (int u = 0; u < NC; u++) {
                    if (i == u || j == u || k == u) { continue; }
                    Tp &vju = vs[j][u];
                    int cp1 = CrossProductZ(vij, vju);
                    Tp &vku = vs[k][u];
                    int cp2 = CrossProductZ(vjk, vku);
                    if (cp1 != cp2) { continue; }
                    Tp &viu = vs[i][u];
                    int cp3 = CrossProductZ(vki, viu);
                    if (cp1 != cp3) { continue; }
                    ok = false;
                    break;
                }
                if (ok == false) { continue; }
                TpD v1 = Rotate(vs[i][j], citiesPos[i]);
                TpD v2 = Rotate(vs[j][i], citiesPos[j]);
                auto d1 = DistD(citiesPos[k], v1);
                auto d2 = DistD(citiesPos[k], v2);
                TpD v3 = Rotate(vs[j][k], citiesPos[j]);
                TpD v4 = Rotate(vs[k][j], citiesPos[k]);
                auto d3 = DistD(citiesPos[i], v3);
                auto d4 = DistD(citiesPos[i], v4);
                if (d2 > d1) { v1 = v2; }
                if (d4 > d3) { v3 = v4; }
                Pos ip = Intersection(v1, citiesPos[k], v3, citiesPos[i]);
                if (used.find(ip) != used.end()) { continue; }
                double c1 = 0.0, c2 = 0.0;
                c1 += double(rmap[i][j]) * citiesDist[i][j];
                c1 += double(rmap[j][k]) * citiesDist[j][k];
                c1 += double(rmap[k][i]) * citiesDist[k][i];
                c2 += Dist(ip, citiesPos[i]);
                c2 += Dist(ip, citiesPos[j]);
                c2 += Dist(ip, citiesPos[k]);
                ret.push_back({c2 - c1, {{{ip, i}, {j, k}}, c2}});
                cnt += 1;
            } 
        }
    }
    cerr << "fermat points: "
         << cnt << " / " << (NC * (NC - 1) * (NC - 2) / 6) << endl;
    return ret;
}

double RoadsAndJunctions::CalcScore() {
    auto flag = new bool[NC + NJ];
    fill(flag, flag + (NC + NJ), false);
    auto minPairT = FindMinPair(dummy);
    Tp minPair = minPairT.first;
    double score = minPairT.second;
    flag[minPair.first] = true;
    flag[minPair.second] = true;
    for (;;) {
        int s1 = -1, s2 = -1;
        double md = 1e10;
        for (int i = 0; i < NC; i++) {
            if (!flag[i]) { continue; }
            for (int j = 0; j < NC; j++) {
                if (flag[j] || i == j) { continue; }
                if (citiesDist[i][j] < md) {
                    s1 = i;
                    s2 = j;
                    md = citiesDist[i][j];
                }
            }
        }
        for (int i = 0; i < NJ; i++) {
            // if (junctionStatus[i] == 0) { continue; }
            bool fi = flag[NC + i];
            auto &ds = juncDist[i];
            for (int j = 0; j < NC; j++) {
                if (flag[j] == fi) { continue; }
                if (ds[j] < md) {
                    s1 = j;
                    s2 = NC + i;
                    md = ds[j];
                }
            }
            for (int j = NC; j < ds.size(); j++) {
                // if (junctionStatus[j - NC] == 0) { continue; }
                if (flag[j] == fi) { continue; }
                if (ds[j] < md) {
                    s1 = j;
                    s2 = NC + i;
                    md = ds[j];
                }
            }
        }
        if (s1 < 0) { break; }
        flag[s1] = true;
        flag[s2] = true;
        score += md;
    }
    delete [] flag;
    return score;
}

